Midnight Mansion is a game we are making for GBJam 2019, it's being made using
GB Studio for the Game Boy.

It's an RPG where you explore an abandoned mansion and do quests for ghosts and 
other creatures.